#!/bin/bash

# Variables
workdir=/opt/scripts/zabbix/mdraid
resultdir=$workdir/result
zbxdiscovery=$resultdir/zbxdiscovery

# Begin discovery file
mkdir -p $resultdir
echo "{\"data\":[" > $zbxdiscovery

# Get info about arrays
mdstat=`cat /proc/mdstat`

# If arrays existing: get names and states
check=`echo "${mdstat}" | grep md | wc -l`
if [ "$check" -ne "0" ]; then
  for mdarray in `echo "${mdstat}" | grep md | awk '{print $1}'`; do
    echo "{ \"{#MDARRAY}\":\"$mdarray\" }, " >> $zbxdiscovery
    check_array=`echo "${mdstat}" |grep -A1 $mdarray |grep _ |wc -l`
    if [ "$check_array" -eq "0" ]; then
      echo "1" > $resultdir/$mdarray
    else
      echo "0" > $resultdir/$mdarray
    fi
  done
fi

# Finish discovery file
echo "]}" >> $zbxdiscovery
zbxdiscoverytmp=`cat $zbxdiscovery |tr -d '\r\n' |sed 's/\(.*\),/\1/'`
echo $zbxdiscoverytmp > $zbxdiscovery
